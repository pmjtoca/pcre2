// Copyright 2019 The pcre2 Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE file.

// Copyright the Perl Compatible Regular Expressions authors. All right reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENCE-PCRE file.

// Package pcre2 is a library of functions to support regular expressions whose
// syntax and semantics are as close as possible to those of the Perl 5
// language.
package pcre2

import (
	"errors"
	"fmt"
	"unsafe"

	"modernc.org/crt"
	"modernc.org/pcre2/internal/pcre"
)

var (
	pcreErrors = map[int32]error{
		pcre.DPCRE2_ERROR_BADMAGIC:       errors.New("bad magic"),
		pcre.DPCRE2_ERROR_BADMODE:        errors.New("bad mode"),
		pcre.DPCRE2_ERROR_BADOFFSET:      errors.New("bad offset"),
		pcre.DPCRE2_ERROR_BADOPTION:      errors.New("bad option"),
		pcre.DPCRE2_ERROR_CALLOUT:        errors.New("callout"),
		pcre.DPCRE2_ERROR_DFA_BADRESTART: errors.New("dfa bad restart"),
		pcre.DPCRE2_ERROR_DFA_RECURSE:    errors.New("dfa recurse"),
		pcre.DPCRE2_ERROR_DFA_UCOND:      errors.New("dfa u cond"),
		pcre.DPCRE2_ERROR_DFA_UITEM:      errors.New("dfa u item"),
		pcre.DPCRE2_ERROR_DFA_WSSIZE:     errors.New("dfa wssize"),
		pcre.DPCRE2_ERROR_INTERNAL:       errors.New("internal"),
		pcre.DPCRE2_ERROR_JIT_BADOPTION:  errors.New("jit bad option"),
		pcre.DPCRE2_ERROR_JIT_STACKLIMIT: errors.New("jit stack limit"),
		pcre.DPCRE2_ERROR_MATCHLIMIT:     errors.New("match limit"),
		pcre.DPCRE2_ERROR_NOMATCH:        errors.New("no match"),
		pcre.DPCRE2_ERROR_NOMEMORY:       errors.New("no memory"),
		pcre.DPCRE2_ERROR_NOSUBSTRING:    errors.New("no substring"),
		pcre.DPCRE2_ERROR_NULL:           errors.New("null"),
		pcre.DPCRE2_ERROR_PARTIAL:        errors.New("partial"),
		pcre.DPCRE2_ERROR_RECURSELOOP:    errors.New("recurse loop"),
		pcre.DPCRE2_ERROR_RECURSIONLIMIT: errors.New("recursion limit"),
		pcre.DPCRE2_ERROR_UNSET:          errors.New("unset"),
	}
)

type Option int

type Regexp struct{ uintptr }

func Compile(pattern string, options Option) (*Regexp, error) {
	//  SYNOPSIS
	//
	//         #include <pcre2.h>
	//
	//         pcre2_code *pcre2_compile(PCRE2_SPTR pattern, PCRE2_SIZE length,
	//           uint32_t options, int *errorcode, PCRE2_SIZE *erroroffset,
	//           pcre2_compile_context *ccontext);
	//
	//  DESCRIPTION
	//
	//         This function compiles a regular expression pattern into an
	//         internal form. Its arguments are:
	//
	//           pattern       A string containing expression to be compiled
	//           length        The length of the string or PCRE2_ZERO_TERMINATED
	//           options       Option bits
	//           errorcode     Where to put an error code
	//           erroffset     Where to put an error offset
	//           ccontext      Pointer to a compile context or NULL
	//
	//         The  length  of  the pattern and any error offset that is
	//         returned are in code units, not characters. A compile
	//         context is needed only if you want to provide custom memory
	//         allocation functions, or to provide an external function for
	//         system stack size checking, or to change one or more of
	//         these parameters:
	//
	//           What \R matches (Unicode newlines, or CR, LF, CRLF only);
	//           PCRE2's character tables;
	//           The newline character sequence;
	//           The compile time nested parentheses limit;
	//           The maximum pattern length (in code units) that is allowed.
	//           The additional options bits (see pcre2_set_compile_extra_options())
	//
	//         The option bits are:
	//
	//           PCRE2_ANCHORED           Force pattern anchoring
	//           PCRE2_ALLOW_EMPTY_CLASS  Allow empty classes
	//           PCRE2_ALT_BSUX           Alternative handling of \u, \U, and \x
	//           PCRE2_ALT_CIRCUMFLEX     Alternative handling of ^ in multiline mode
	//           PCRE2_ALT_VERBNAMES      Process backslashes in verb names
	//           PCRE2_AUTO_CALLOUT       Compile automatic callouts
	//           PCRE2_CASELESS           Do caseless matching
	//           PCRE2_DOLLAR_ENDONLY     $ not to match newline at end
	//           PCRE2_DOTALL             . matches anything including NL
	//           PCRE2_DUPNAMES           Allow duplicate names for subpatterns
	//           PCRE2_ENDANCHORED        Pattern can match only at end of subject
	//           PCRE2_EXTENDED           Ignore white space and # comments
	//           PCRE2_FIRSTLINE          Force matching to be before newline
	//           PCRE2_LITERAL            Pattern characters are all literal
	//           PCRE2_MATCH_UNSET_BACKREF  Match unset back references
	//           PCRE2_MULTILINE          ^ and $ match newlines within data
	//           PCRE2_NEVER_BACKSLASH_C  Lock out the use of \C in patterns
	//           PCRE2_NEVER_UCP          Lock out PCRE2_UCP, e.g. via (*UCP)
	//           PCRE2_NEVER_UTF          Lock out PCRE2_UTF, e.g. via (*UTF)
	//           PCRE2_NO_AUTO_CAPTURE    Disable numbered capturing paren-
	//                                     theses (named ones available)
	//           PCRE2_NO_AUTO_POSSESS    Disable auto-possessification
	//           PCRE2_NO_DOTSTAR_ANCHOR  Disable automatic anchoring for .*
	//           PCRE2_NO_START_OPTIMIZE  Disable match-time start optimizations
	//           PCRE2_NO_UTF_CHECK       Do not check the pattern for UTF validity
	//                                      (only relevant if PCRE2_UTF is set)
	//           PCRE2_UCP                Use Unicode properties for \d, \w, etc.
	//           PCRE2_UNGREEDY           Invert greediness of quantifiers
	//           PCRE2_USE_OFFSET_LIMIT   Enable offset limit for unanchored matching
	//           PCRE2_UTF                Treat pattern and subjects as UTF strings
	//
	//         PCRE2 must be built with Unicode support (the default) in order to use PCRE2_UTF, PCRE2_UCP and related options.
	//
	//         The yield of the function is a pointer to a private data structure that contains the compiled pattern, or NULL if an error was detected.

	type out struct {
		errptr    uintptr
		erroffset pcre.Tsize_t
	}

	cpattern := crt.MustCString(pattern)
	o := crt.MustMalloc(int(unsafe.Sizeof(out{})))
	tls := crt.TLS(crt.MustMalloc(int(unsafe.Sizeof(crt.Thread{}))))

	defer func() {
		crt.Free(cpattern)
		crt.Free(o)
		tls.Release()
	}()

	r := pcre.Xpcre2_compile_8(tls, cpattern, pcre.Tsize_t(len(pattern)), uint32(options), o+unsafe.Offsetof(out{}.errptr), o+unsafe.Offsetof(out{}.erroffset), 0)
	if r == 0 {
		eo := *(*out)(unsafe.Pointer(o))
		return nil, fmt.Errorf("offset %v: %s", eo.erroffset, crt.GoString(eo.errptr))
	}

	return &Regexp{r}, nil
}

// MustCompile is like Compile but panics if the expression cannot be parsed.
// It simplifies safe initialization of global variables holding compiled regular
// expressions.
func MustCompile(s string, options Option) *Regexp {
	r, err := Compile(s, options)
	if err != nil {
		panic(fmt.Errorf("pcre: Compile(%q): %v", s, err))
	}

	return r
}

func (re *Regexp) free() {
	// SYNOPSIS
	//
	//        #include <pcre2.h>
	//
	//        void pcre2_code_free(pcre2_code *code);
	//
	// DESCRIPTION
	//
	//        This  function  frees the memory used for a compiled pattern,
	//        including any memory used by the JIT compiler. If the
	//        compiled pattern was created by a call to
	//        pcre2_code_copy_with_tables(), the memory for the character
	//        tables is also freed.

	tls := crt.TLS(crt.MustMalloc(int(unsafe.Sizeof(crt.Thread{}))))

	defer tls.Release()

	pcre.Xpcre2_code_free_8(tls, re.uintptr)
	re.uintptr = 0
}

func (re *Regexp) Exec(subject []byte, options Option, out []uint64) (int, error) {
	//  SYNOPSIS
	//
	//         #include <pcre2.h>
	//
	//         int pcre2_match(const pcre2_code *code, PCRE2_SPTR subject,
	//           PCRE2_SIZE length, PCRE2_SIZE startoffset,
	//           uint32_t options, pcre2_match_data *match_data,
	//           pcre2_match_context *mcontext);
	//
	//  DESCRIPTION
	//
	//         This  function  matches a compiled regular expression
	//         against a given subject string, using a matching algorithm
	//         that is similar to Perl's. It returns offsets to what it has
	//         matched and to captured substrings via the match_data block,
	//         which can be processed by functions with names that start
	//         with pcre2_get_ovector_...() or pcre2_substring_...(). The
	//         return from pcre2_match() is  one  more than the highest
	//         numbered capturing pair that has been set (for example, 1 if
	//         there are no captures), zero if the vector of offsets is too
	//         small, or a negative error code for no match and other
	//         errors. The function arguments are:
	//
	//           code         Points to the compiled pattern
	//           subject      Points to the subject string
	//           length       Length of the subject string
	//           startoffset  Offset in the subject at which to start matching
	//           options      Option bits
	//           match_data   Points to a match data block, for results
	//           mcontext     Points to a match context, or is NULL
	//
	//         A match context is needed only if you want to:
	//
	//           Set up a callout function
	//           Set a matching offset limit
	//           Change the heap memory limit
	//           Change the backtracking match limit
	//           Change the backtracking depth limit
	//           Set custom memory management specifically for the match
	//
	//         The length and startoffset values are code units, not
	//         characters. The length may be given as PCRE2_ZERO_TERMINATE
	//         for a subject that is terminated by a binary zero code unit.
	//         The  options are:
	//
	//           PCRE2_ANCHORED          Match only at the first position
	//           PCRE2_ENDANCHORED       Pattern can match only at end of subject
	//           PCRE2_NOTBOL            Subject string is not the beginning of a line
	//           PCRE2_NOTEOL            Subject string is not the end of a line
	//           PCRE2_NOTEMPTY          An empty string is not a valid match
	//           PCRE2_NOTEMPTY_ATSTART  An empty string at the start of the subject
	//                                    is not a valid match
	//           PCRE2_NO_JIT            Do not use JIT matching
	//           PCRE2_NO_UTF_CHECK      Do not check the subject for UTF
	//                                    validity (only relevant if PCRE2_UTF
	//                                    was set at compile time)
	//           PCRE2_PARTIAL_HARD      Return PCRE2_ERROR_PARTIAL for a partial
	//                                    match even if there is a full match
	//           PCRE2_PARTIAL_SOFT      Return PCRE2_ERROR_PARTIAL for a partial
	//                                     match if no full matches are found

	var s uintptr
	if len(subject) != 0 {
		s = uintptr(unsafe.Pointer(&subject[0]))
	}

	tls := crt.TLS(crt.MustMalloc(int(unsafe.Sizeof(crt.Thread{}))))

	defer tls.Release()

	md := pcre.Xpcre2_match_data_create_8(tls, uint32(len(out)), 0)
	if md == 0 {
		panic("TODO")
	}

	defer pcre.Xpcre2_match_data_free_8(tls, md)

	switch rc := pcre.Xpcre2_match_8(tls, re.uintptr, s, pcre.Tsize_t(len(subject)), 0, uint32(options), md, 0); {
	case rc < 0:
		if rc == pcre.DPCRE2_ERROR_NOMATCH {
			return 0, nil
		}

		return 0, re.error(rc)
	case rc == 0:
		return 0, fmt.Errorf("%T.Exec: vector of offsets is too small", re)
	default:
		copy(out[:2*rc], (*pcre.Spcre2_real_match_data_8)(unsafe.Pointer(md)).Fovector[:])
		return int(rc), nil
	}
}

func (re *Regexp) error(rc int32) error {
	if err, ok := pcreErrors[rc]; ok {
		return err
	}

	return fmt.Errorf("unknown error code: %v", rc)
}

func (re *Regexp) Fullinfo( /*TODO extra_data uintptr, */ what uint32) (uint32, error) {
	switch what {
	case pcre.DPCRE2_INFO_CAPTURECOUNT:
		o := crt.MustMalloc(int(unsafe.Sizeof(uint32(0))))
		tls := crt.TLS(crt.MustMalloc(int(unsafe.Sizeof(crt.Thread{}))))

		defer func() {
			crt.Free(o)
			tls.Release()
		}()

		rc := pcre.Xpcre2_pattern_info_8(tls, re.uintptr, what, o)
		if rc != 0 {
			panic(rc) //TODO
		}

		return *(*uint32)(unsafe.Pointer(o)), nil
	default:
		return 0, fmt.Errorf("%T.Fullinfo: unsupported 'what': %v", re, what)
	}
}

func (re *Regexp) FindIndex(b []byte, options Option) (loc []uint64, err error) {
	ng, err := re.Fullinfo(pcre.DPCRE2_INFO_CAPTURECOUNT)
	if err != nil {
		return nil, err
	}

	out := make([]uint64, (ng+1)*3)
	n, err := re.Exec(b, options, out) //TODO is there an option like 'ignore submatches'?
	if err != nil {
		return nil, err
	}

	if n == 0 || out[0] == 0 && out[1] == 0 {
		return nil, nil
	}

	return out[:2:2], nil
}

func (re *Regexp) ReplaceAll(b, repl []byte, options Option) (r []byte, err error) {
	ng, err := re.Fullinfo(pcre.DPCRE2_INFO_CAPTURECOUNT)
	if err != nil {
		return nil, err
	}

	out := make([]uint64, (ng+1)*3)
	for len(b) != 0 {
		n, err := re.Exec(b, options, out) //TODO is there an option like 'ignore submatches'?
		if err != nil {
			return nil, err
		}

		if n == 0 {
			break
		}

		first := out[0]
		next := out[1]
		// abc<first>mno<next>xyz
		r = append(append(r, b[:first]...), repl...)
		b = b[next:]
	}
	return append(r, b...), nil
}
