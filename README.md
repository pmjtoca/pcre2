# Perl Compatible Regular Expressions

pcre2-10.32 without CGo. Handles UTF{8,16,32} and Unicode properties.

### Supported operating systems and architectures

- linux/amd64
- linux/386

More OS/arch support is planned.

### Installation

To install or update pcre2

     $ go get [-u] modernc.org/pcre2

Documentation: [godoc.org/modernc.org/pcre2](http://godoc.org/modernc.org/pcre2)

